var Header = require("../react_components/header.jsx");
var UserMixin = require("../react_components/user-mixin.jsx");
var Firebase = require("firebase");
var React = require("react");
var ReactDOM = require("react-dom");
var ReactFireMixin = require("reactfire");
var ReactBootstrap = require("react-bootstrap");
var Table = ReactBootstrap.Table;

var FollowingRows = React.createClass({
  render: function() {
    var createRow = function(row, index) {
        var uri = "/chart/"+row.chart_id;
        return (
            <tr key={index}>
                <td>{row.owner_name}</td>
                <td><a href={uri}>{uri}</a></td>
            </tr>
        );
    };
    return <tbody>{ this.props.rows.map(createRow) }</tbody>;
  }
});

var FollowingTable = React.createClass({
  mixins: [ReactFireMixin, UserMixin],
  
  componentWillMount: function(){
    var that = this;
    var followingRef = new Firebase('https://betachains.firebaseio.com/followers/').orderByChild("user_id").startAt(this.state.user_id).endAt(this.state.user_id);
    this.bindAsArray(followingRef,"following");
    followingRef.once("value",function(snapshot){
      var result = snapshot.val();
      var following = [];
      for(var i in result){
        var ownerRef = new Firebase('https://betachains.firebaseio.com/users/'+result[i].owner_id);
        ownerRef.once("value",function(snapshot){
          var owner = snapshot.val().name;
          following.push({owner_name: snapshot.val().name,chart_id:result[i].chart_id});
          that.setState({following: following});
        });
      }
    });
  },

  render: function(){
    return ( 
        <Table hover>
            <thead>
               <tr>
                   <th>Username</th>
                   <th>Link to chart</th>
               </tr>
           </thead>
           <FollowingRows rows={this.state.following}/>
        </Table>
    );
  }
});

ReactDOM.render(<Header/>, document.getElementById('header'));
ReactDOM.render(<FollowingTable/>, document.getElementById('following-table'));