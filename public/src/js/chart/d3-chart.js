var d3 = require("d3");

//defining a class for the chart to keep track of scope and run some ajax-y calls on the objects
var D3Chart = function(el){
    var self = this;
    self.el = el;
    //basic dimension inits
    self.margin = {top: 20, right: 20, bottom: 40, left: 60},
    self.width = 960 - self.margin.left - self.margin.right,
    self.height = 500 - self.margin.top - self.margin.bottom;

    // x-axis is dates so need to parse incoming data, formating
    self.parseDate = d3.time.format("%Y-%m-%d %H:%M:%S").parse,
    self.formatDate = d3.time.format("%m/%d/%Y %H:%M");
    
    //define scaling for x & y values, called everytime a zoom takes place
    self.x = d3.time.scale()
        .range([0, self.width]);
    
    self.y = d3.scale.linear()
        .range([self.height, 0]);
       
    //define x & y axis 
    self.xAxis = d3.svg.axis()
        .scale(self.x)
        .orient("bottom")
        .tickSize(-self.height, 0)
        .tickPadding(6);
    
    self.yAxis = d3.svg.axis()
        .scale(self.y)
        .orient("left")
        .tickSize(self.width,0)
        .tickPadding(6);
    
    // init the div for the tooltip
    self.tooltip = d3.select("body").append("div")	
        .attr("class", "tooltip")				
        .style("opacity", 0);
    
    //line/path draw function
    self.line = d3.svg.line()
        .x(function(d) { return self.x(d.date); })
        .y(function(d) { return self.y(d.value); });
    
    // main svg init
    self.svg = d3.select("#"+el).append("svg")
        .attr("width", self.width + self.margin.left + self.margin.right)
        .attr("height", self.height + self.margin.top + self.margin.bottom)
      .append("g")
        .attr("transform", "translate(" + self.margin.left + "," + self.margin.top + ")");

    // draw/re-draw/update on zoom, smaller functions that parse values based on new width & height
    self.draw = function() {
        self.svg.select("g.x.axis").call(self.xAxis);
        self.svg.select("g.y.axis").call(self.yAxis);
        self.svg.select("path.line").attr("d", self.line);
        self.svg.selectAll("circle")           
               .attr("cx", function(d) { return self.x(d.date); })                              
               .attr("cy", function(d) { return self.y(d.value); }); 
      }
     
    //zoom calls draw
    self.zoom = d3.behavior.zoom().on("zoom", self.draw);

    //defines boundaries of chart content, after panning it will display hidden sections
    self.svg.append("clipPath")
        .attr("id", "clip")
      .append("rect")
        .attr("x", self.x(0))
        .attr("y", self.y(1))
        .attr("width", self.width)
        .attr("height", self.height)

    //x & y axis append to svg
    self.svg.append("g")
        .attr("class", "y axis")
        .attr("transform", "translate(" + self.width + ",0)")

    self.svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + self.height + ")")
        
    // text label for the y axis
    self.svg.append("text")     
        .attr("class", "axis-label")
        .text("Blood Glucose")
        .style("text-anchor","middle")
        .attr("transform","translate(" + -40 + " " + self.height/2+") rotate(-90)"); 

    // text label for the x axis
    self.svg.append("text")      
        .attr("class", "axis-label")
        .text("Date & Time")
        .attr("x", self.width/2)
        .attr("y", self.height)
        .attr("dy","2.4em")
        .style("text-anchor","middle");
        
    //main chart rectangle append to svg
    self.svg.append("rect")
        .attr("class", "pane")
        .attr("width", self.width)
        .attr("height", self.height)
        .call(self.zoom);

    //container for main chart line/path and data points (circles), adding clip-path to prevent bleeding over edges of axis
    self.g = self.svg.append("g").attr("clip-path", "url(#clip)");

    self.g.append("path")
        .attr("class", "line");
}
module.exports = D3Chart;