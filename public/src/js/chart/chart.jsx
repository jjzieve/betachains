var Utils = require("../utils");
var Header = require("../react_components/header.jsx");
var UserMixin = require("../react_components/user-mixin.jsx");
var ChartMixin = require("../react_components/chart-mixin.jsx");
var Firebase = require("firebase");
var React = require("react");
var ReactDOM = require("react-dom");
var ReactFireMixin = require("reactfire");
var ReactBootstrap = require("react-bootstrap");
var sweetAlert = require("sweetalert");
var d3 = require("d3");
var D3Chart = require("./d3-chart");
var dateFormat = require('dateformat');
var DateTimeField = require('react-bootstrap-datetimepicker');

var Modal = ReactBootstrap.Modal;
var Button = ReactBootstrap.Button;
var ButtonGroup = ReactBootstrap.ButtonGroup;
var Input = ReactBootstrap.Input;

//Should I keep a local copy of user & owner? Ajaxing seems weird here, but it may be the safest (security wise)...
var OwnerMixin = {
  getInitialState: function() {
   return {
     owner_id: "",
     owner: {}
   }
  },
  // have to use "didMount" instead of "willMount" b/c "ajaxing" to get owner_id... see this -> https://facebook.github.io/react/tips/initial-ajax.html
  componentDidMount: function() {
    //gleaming the chart_id from path is a temp way to manage state from the url rather than cookies or something
    var reference = new Firebase('https://betachains.firebaseio.com');
    var path = window.location.pathname;
    var chart_id = path.substr(path.lastIndexOf('/') + 1);
    var chart_reference = new Firebase('https://betachains.firebaseio.com/charts/'+chart_id);
    var that = this;
    chart_reference.once("value", function(snapshot) {
      var owner_id = snapshot.val().owner;
      var owner_reference = reference.child("users").child(owner_id);
      owner_reference.once("value", function(snapshot) {
        if (that.isMounted()) {
          that.setState({
            owner_id: owner_id,
            owner: snapshot.val()
          });
        }
      });
    });
  }
}

var ModalMixin = {
  //provides a callback to the parent to change the modals state to closed
  close: function() {
    this.props.onClose();
  }
}

/** Components **/
//Chart header components
var BloodGlucoseModal = React.createClass({
  mixins: [ReactFireMixin, ChartMixin, OwnerMixin, UserMixin, ModalMixin],

  componentWillMount: function() {
    var that = this;
    var dataRef = new Firebase('https://betachains.firebaseio.com/data/');
    this.bindAsObject(dataRef, 'data');
    //followers mixin?
    var followersRef = new Firebase('https://betachains.firebaseio.com/followers/');
    var chartFollowersRef = followersRef.orderByChild("chart_id").startAt(this.state.chart_id).endAt(this.state.chart_id);
    chartFollowersRef.once("value",function(snapshot){
      var result = snapshot.val();
      var followers = [];
      for(var i in result){
        var userRef = new Firebase('https://betachains.firebaseio.com/users/'+result[i].user_id);
        userRef.once("value",function(snapshot){
          followers.push(snapshot.val());
          that.setState({
            followers: followers
          });
        });
      }
    });
  },
  
  notify: function(bg){
    for(var i in this.state.followers){
      var request = {
        uri: "http://betachains.com/chart/"+this.state.chart_id,
        owner: this.state.owner,
        follower: this.state.followers[i],
        bg: bg
      }
      Utils.ajax_post("/notify",request);
    }
  },

  save: function(){
    // if not int, or not within range, sweet alert
    if(!isNaN(this.refs.bg.getValue()) && !isNaN(this.refs.datetime.getValue())){
      var timestamp = parseInt(this.refs.datetime.getValue());
      var bg = parseInt(this.refs.bg.getValue());
      if(bg > 0 && bg < 400){
        //push data up to firebase, should it be UTC and parse on client side to current timezone or push in current timezone?
        this.firebaseRefs['data'].push({
          chart_id: this.state.chart_id,
          bg: bg,
          timestamp: timestamp
        });
        //good range = [80,120], no need to notify
        if(!(bg >= 80 && bg <= 120)){
          //notify followers if bg is in a bad range
          this.notify(bg);
        }
      }
      else{
        sweetAlert("Number not within 0-400, go to the hospital plz. ", "", "error");
      }
      this.close();
    }
    else{
      sweetAlert("That ain't a number broh.", "", "error");
    }
  },
  
  render: function(){
    return (
      <div>
        <Modal show={this.props.showModal} onHide={this.close}>
          <Modal.Header closeButton>
            <Modal.Title>Sike! That's the wrong numbah!</Modal.Title>
          </Modal.Header>
    
          <Modal.Body>
            <form className="form-horizontal">
              <Input type="text" ref="bg" label="BG" labelClassName="col-xs-2" wrapperClassName="col-xs-10" />
              <DateTimeField label="Datetime" labelClassName="col-xs-2" wrapperClassName="col-xs-10" ref="datetime" />
            </form>
          </Modal.Body>
    
          <Modal.Footer>
            <Button onClick={this.close}>Close</Button>
            <Button onClick={this.save} bsStyle="primary">Save changes</Button>
          </Modal.Footer>
    
        </Modal>
      </div>
    );
  }
});

var InviteModal = React.createClass({
  mixins: [ReactFireMixin, ChartMixin, OwnerMixin, ModalMixin],

  invite: function(){
     var request = {
      owner: this.state.owner,
      email: this.refs.input.getValue(),
      uri: "http://betachains.com/chart/"+this.state.chart_id
    }
    console.log(request);
    Utils.ajax_post("/invite",request);
    this.close();
  },
  
  render: function(){
    return (
      <div>
        <Modal show={this.props.showModal} onHide={this.close}>
          <Modal.Header closeButton>
            <Modal.Title>Enter a loved-one's email, so they can come ridicule you.</Modal.Title>
          </Modal.Header>
    
          <Modal.Body>
            <form className="form-horizontal">
              <Input type="email" ref="input" label="Email" labelClassName="col-xs-2" wrapperClassName="col-xs-10" />
            </form>
          </Modal.Body>
    
          <Modal.Footer>
            <Button onClick={this.close}>Close</Button>
            <Button onClick={this.invite} bsStyle="primary">Send</Button>
          </Modal.Footer>
    
        </Modal>
      </div>
    );
  }
});

var OwnerActionButtons = React.createClass({
  getInitialState: function(){
    return {showBG:false, showInvite: false}
  },
  
  toggleBG: function(){
    this.setState({showBG: !this.state.showBG});
  },
  
  toggleInvite: function(){
    this.setState({showInvite: !this.state.showInvite});
  },
  
  render: function(){
    return(
      <div>
        <ButtonGroup className="pull-right">
          <Button onClick={this.toggleBG} bsStyle="primary">Enter a numbah!</Button>
          <Button onClick={this.toggleInvite} bsStyle="primary">Invite!</Button>
        </ButtonGroup>
        <BloodGlucoseModal showModal={this.state.showBG} onClose={this.toggleBG} />
        <InviteModal showModal={this.state.showInvite}  onClose={this.toggleInvite} />
      </div>
    )
  }
});

var FollowerActionButtons = React.createClass({
  mixins: [ReactFireMixin, ChartMixin, UserMixin, OwnerMixin],
  getInitialState: function(){
    return {follower_id: false}
  },
  
  componentWillMount: function(){
    var that = this;
    var followersRef = new Firebase('https://betachains.firebaseio.com/followers/');
    this.bindAsArray(followersRef, 'followers');
    //not sure which is quicker: search graph followers for user_id or search graph_id in user's following
    var chartFollowersRef = followersRef.orderByChild("chart_id").startAt(this.state.chart_id).endAt(this.state.chart_id);
    chartFollowersRef.once("value",function(snapshot){
      var followers = snapshot.val();
      for(var i in snapshot.val()){
        if(followers[i].user_id === that.state.user_id){
          that.setState({follower_id: i});
        }
      }
    });
  },
  
  handleClick: function() {
    if(this.state.follower_id){
       var followerRef = new Firebase('https://betachains.firebaseio.com/followers/'+this.state.follower_id);
       followerRef.remove();
       this.setState({follower_id: false});
    }
    else{
      var newFollower = 
        this.firebaseRefs['followers'].push({
          chart_id: this.state.chart_id,
          user_id: this.state.user_id,
          owner_id: this.state.owner_id
        });
      this.setState({follower_id: newFollower.key()});
    }
  },
  
  render: function(){
    var text = "Follow";
    var buttonType = "primary"
    if(this.state.follower_id){
      text = "Unfollow";
      buttonType = "danger"
    }

    return <Button onClick={this.handleClick} className="pull-right" bsStyle={ buttonType }>{ text }</Button>;
  }
});

var ChartHeader = React.createClass({
  mixins: [ReactFireMixin, UserMixin, OwnerMixin],
  
  render: function() {
    var actionButtons;
    if(this.state.owner_id === this.state.user_id){//the current user is also the owner of the graph
      actionButtons = <OwnerActionButtons />;
    }
    else{// the current user is just a follower
      actionButtons = <FollowerActionButtons />;
    }
    //inline styles here just to line up buttons with header
    var actionButtonsStyle = {
      position: "absolute",
      bottom:0,
      right:0
    }
    var rowStyle = {
      position: "relative"
    }
    return (
      <div className="row" style={rowStyle}>
        <div className="col-xs-6">
          <h1>{ this.state.owner.name }'s beetus chart</h1>
        </div>
        <div className="col-xs-6" style={actionButtonsStyle}>
          { actionButtons }
        </div>
      </div>
    );
  }
});

var Chart = React.createClass({
  mixins: [ReactFireMixin, ChartMixin],
  render: function(){
    var dataRef = new Firebase('https://betachains.firebaseio.com/data/');
    var chart = new D3Chart("bg-chart");
    var chart_id = this.state.chart_id;
    //grab firebase reference by value, should get the entire list and an updated entire list on adding another bg, could be more efficient to use "child_added"
    dataRef.orderByChild("chart_id").startAt(chart_id).endAt(chart_id).on("value",function(snapshot){
      var fb_data = snapshot.val();
      var data = Object.keys(fb_data).map(function(value, index) {
          var d = fb_data[value];
          d.value = +d.bg;
          var date = new Date(d.timestamp);
          d.date = chart.parseDate(Utils.toMysqlFormat(date));
          return d;
      });
      //define domain (or what the zoom level should be). Today = zoom window, or all data = zoom data?
      var now = new Date();
      var tomorrow = new Date();
      var yesterday = new Date();
      tomorrow.setDate(now.getDate()+1);
      yesterday.setDate(now.getDate()-1);

      chart.x.domain([yesterday,tomorrow]);
      chart.y.domain([0, 400]);
      chart.zoom.x(chart.x);
      
      //attach data to main path & line
      chart.svg.select("path.line").data([data]);
      
      //init circles, attach to "parental" <g> from before
      chart.circles = chart.g.selectAll("circle")
          .data(data)
          .enter()
          .append("circle")
          .attr("cx", function(d) { return chart.x(d.date); })
          .attr("cy", function(d) { return chart.y(d.value); })
          .attr("r", 5)
          //when hovering over circles/dot changed styling and add a tooltip with the granular data at that spot
          .on("mouseover",function(d){
              d3.select(this).classed("selected",true);
              chart.tooltip.transition()		
                  .duration(200)		
                  .style("opacity", .9);		
              chart.tooltip.html("BG: "+d.value + "<br/>" + "Date: "+chart.formatDate(d.date))	
                  .style("left", (d3.event.pageX) + "px")		
                  .style("top", (d3.event.pageY - 28) + "px");	
          })
          .on("mouseout",function(d){
              d3.select(this).classed("selected",false);
              chart.tooltip.transition()		
                  .duration(500)		
                  .style("opacity", 0);	
          });
      
    /* Static horizontal lines to define some bad blood glucose thresholds */
    //a little too high    
    chart.svg.append("line")
        .style("stroke-dasharray", ("3, 3"))
        .style("stroke", "#5bc0de")
        .attr("x1", 0)  
        .attr("y1", chart.y(120))
        .attr("x2", chart.width)
        .attr("y2", chart.y(120));
    
    //too high    
    chart.svg.append("line")
        .style("stroke-dasharray", ("3, 3"))
        .style("stroke", "orange")
        .attr("x1", 0)  
        .attr("y1", chart.y(200))
        .attr("x2", chart.width)
        .attr("y2", chart.y(200));
        
    //way too high!
    chart.svg.append("line")
        .style("stroke-dasharray", ("3, 3"))
        .style("stroke", "red")
        .attr("x1", 0)  
        .attr("y1", chart.y(300))
        .attr("x2", chart.width)
        .attr("y2", chart.y(300));
        
    //too low 
    chart.svg.append("line")
        .style("stroke-dasharray", ("3, 3"))
        .style("stroke", "gray")
        .attr("x1", 0)  
        .attr("y1", chart.y(80))
        .attr("x2", chart.width)
        .attr("y2", chart.y(80));
    
      //finally, call initial draw()
      chart.draw();
    });
    return (
      <div className="Chart"></div>
    );
  }
})

// //Chat components
var MessageList = React.createClass({
  render: function() {
    var _this = this;
    var createMessage = function(message, index) {
      var userNameStyle = {
        color: message.color,
        fontWeight: "bold" 
      };
      var date = dateFormat(new Date(message.timestamp),"dddd, mmmm dS, yyyy, h:MM:ss TT");
      return (
        <li key={ index }>
          <span style={ userNameStyle }>{ message.author }</span>: { message.text }
          <div className="pull-right">
            { date }
          </div>
        </li>
      );
    };
    return <ul className="messages">{ this.props.messages.map(createMessage) }</ul>;
  }
});

var Chat = React.createClass({
  mixins: [ReactFireMixin, UserMixin, ChartMixin, OwnerMixin],
  getInitialState: function() {
    return {
      messages: [],
      text: ''
    };
  },
  componentWillMount: function() {
    var that = this;
    var messagesRef = new Firebase('https://betachains.firebaseio.com/messages/');
    //we only want the messages from this particular chart/chatroom
    var chartMessagesRef = messagesRef.orderByChild("chart_id").startAt(this.state.chart_id).endAt(this.state.chart_id);
    this.bindAsArray(chartMessagesRef, 'messages');
    var followersRef = new Firebase('https://betachains.firebaseio.com/followers/');
    var chartFollowersRef = followersRef.orderByChild("chart_id").startAt(this.state.chart_id).endAt(this.state.chart_id);
    chartFollowersRef.once("value",function(snapshot){
      var result = snapshot.val();
      var followers = [];
      for(var i in result){
        var userRef = new Firebase('https://betachains.firebaseio.com/users/'+result[i].user_id);
        userRef.once("value",function(snapshot){
          followers.push(snapshot.val());
          that.setState({followers: followers});
        });
      }
    });
  },
 
  notify: function(comment){
    //send notifications to all the followers
    for(var i in this.state.followers){
      var request = {
        uri: "http://betachains.com/chart/"+this.state.chart_id,
        owner: this.state.owner,
        user: this.state.user,
        follower: this.state.followers[i],
        comment: comment
      }
      Utils.ajax_post("/notify_comment",request);
    }
    //also send a notification to the owner
    var request = {
        uri: "http://betachains.com/chart/"+this.state.chart_id,
        owner: this.state.owner,
        user: this.state.user,
        follower: this.state.owner,
        comment: comment
    }
    Utils.ajax_post("/notify_comment",request);
  },
  
  onChange: function(e) {
    this.setState({text: e.target.value});
  },
  handleSubmit: function(e) {
    e.preventDefault();
    if (this.state.text && this.state.text.trim().length !== 0) {
      this.firebaseRefs['messages'].push({
        chart_id: this.state.chart_id,
        author: this.state.user.name,
        color: this.state.user.chat_color,
        text: this.state.text,
        timestamp: Firebase.ServerValue.TIMESTAMP
      });
      this.notify(this.state.text);
      this.setState({
        text: ''
      });
    }
  },
  render: function() {
    return (
      <div>
        <MessageList messages={ this.state.messages } />
            <form onSubmit={ this.handleSubmit }>
                <div className="input-group">
                    <input className="form-control" autoComplete="off" onChange={ this.onChange } value={ this.state.text } />
                    <span className="input-group-btn">
                        <button className="btn btn-primary">{ 'Shame! #' + (this.state.messages.length + 1) }</button>
                    </span>
                </div>
            </form>
      </div>
    );
  }
});


/** Render **/
//Should maybe just add an App component
ReactDOM.render(<Header/>, document.getElementById('header'));
ReactDOM.render(<ChartHeader/>, document.getElementById('chart-header'));
ReactDOM.render(<Chart/>, document.getElementById('bg-chart'));
ReactDOM.render(<Chat/>, document.getElementById('chat'));
