var Firebase = require("firebase");
var reference = new Firebase("https://betachains.firebaseio.com");
reference.onAuth(function(authentication_data) {
    if (authentication_data) {
        console.log("Authenticated with uid:", authentication_data.uid);
        //if current page is login, redirect to relevant home, specific to user type
        if(window.location.pathname === "/login"){
            var user_reference = reference.child("users").child(authentication_data.uid);
            user_reference.once("value",function(snapshot){
                var user = snapshot.val();
                //if diabetic, go to their chart
                if(user.is_diabetic){
                    window.location = "/chart/"+user.chart_id;
                }//else go to the page showing who they're following
                else{
                    window.location = "/following";
                }
            });
        }
    } 
    else {
        console.log("Client unauthenticated.");
        //can only visit login or register while unauthenticated
        // should save redirect path though, and forward after authentication
        if(window.location.pathname !== "/login" && window.location.pathname !== "/register"){
            window.location = "/login?redirect="+window.location.pathname;
        }
    }
});