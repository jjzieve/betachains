var Utils = {
  ajax_post: function(uri,request){
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function (data) {
       if (ajax.readyState==4 && ajax.status==200){
           console.log("Successful post!");
       }
    }
    // ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
    ajax.open('POST', uri);
    ajax.setRequestHeader("Content-Type", "application/json");
    ajax.send(JSON.stringify(request));
  },

  //helpers to get mysql d from js Date() object, should take out of global
  twoDigits: function(d) {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
  },
  
  toMysqlFormat: function(d) {
    return d.getFullYear() + "-" + this.twoDigits(1 + d.getMonth()) + "-" + this.twoDigits(d.getDate()) + " " + this.twoDigits(d.getHours()) + ":" + this.twoDigits(d.getMinutes()) + ":" + this.twoDigits(d.getSeconds());
  }
}

module.exports = Utils;
