var Firebase = require("firebase");
var ChartMixin = {
  getInitialState: function() {
    var path = window.location.pathname;
    var chart_id = path.substr(path.lastIndexOf('/') + 1);
    return {
      chart_id: chart_id,
      chart: {},
    };
  },
  componentWillMount: function() {
    var chartRef = new Firebase('https://betachains.firebaseio.com/charts/'+this.state.chart_id);
    this.bindAsObject(chartRef, 'chart');
  }
}
module.exports = ChartMixin;