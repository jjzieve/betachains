var Firebase = require("firebase");
var UserMixin = {
  getInitialState: function() {
    var auth = JSON.parse(localStorage.getItem("firebase:session::betachains"));
    var user_id = (auth === null) ? null : auth.uid;
    return {
      //faster than calling firebase.getAuth(), put may break shared state between client and firebase
      user_id: user_id,
      user: {}
    };
  },
  //should this just be in initial state because it won't be updated?
  componentWillMount: function() {
    var userRef = new Firebase('https://betachains.firebaseio.com/users/'+this.state.user_id);
    this.bindAsObject(userRef, 'user');
  }
};
module.exports = UserMixin;