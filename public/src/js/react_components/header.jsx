var React = require("react");
var Firebase = require("firebase");
var ReactFireMixin = require("reactfire");
var ReactBootstrap = require("react-bootstrap");
var Nav = ReactBootstrap.Nav;
var Navbar = ReactBootstrap.Navbar;
var NavDropdown = ReactBootstrap.NavDropdown;
var NavItem = ReactBootstrap.NavItem;
var MenuItem = ReactBootstrap.MenuItem;
var UserMixin = require("./user-mixin.jsx");

var LoggedOut = React.createClass({
  render: function(){
    return <NavItem eventKey={1} href="/login">Login</NavItem>;
  }
});

var LoggedIn = React.createClass({
  handleSelect: function(event, eventKey){
    switch(eventKey){
      case "logout":
        this.logout();
        break;
      case "following":
        this.following();
        break;
      case "settings":
        this.settings();
        break;
      case "chart":
        this.chart();
        break;
      default:
        break;
    }
  },
  logout: function(){
    var reference = new Firebase("https://betachains.firebaseio.com");
    reference.unauth();
    window.location = "/login";
  },
  following: function(){
    window.location = "/following";
  },
  settings: function(){
    window.location = "/settings";
  },
  chart: function(){
    window.location = "/chart/"+this.props.user.chart_id;
  },
  render: function(){
    var myChartLinkStyle = {}
    //chart link won't show if they aren't diabetic 
    if(!this.props.user.is_diabetic){
      myChartLinkStyle["display"] = "none";
    }
    return (
      <NavDropdown id="header-dropdown" eventKey={1} title={"Sup, " + this.props.user.name+ "!"} onSelect={this.handleSelect}>
        <MenuItem style={myChartLinkStyle} eventKey="chart">My Chart</MenuItem>
        <MenuItem eventKey="following">Who I'm following</MenuItem>
        <MenuItem eventKey="settings">Settings</MenuItem>
        <MenuItem divider />
        <MenuItem eventKey="logout">Logout</MenuItem>
      </NavDropdown>
    )
  }
});

var Header = React.createClass({
  mixins: [ReactFireMixin, UserMixin],
  render: function(){
    var header_content;
    //if user_id is null, user isn't authenticated
    if(this.state.user_id === null){
      header_content = <LoggedOut/>
    }
    else{
      header_content = <LoggedIn user={this.state.user} />
    }
    return (
      <Navbar brand="Betachains" toggleNavKey={0}>
        <Nav right eventKey={0}>
          { header_content }
        </Nav>
      </Navbar>
    );
  }
});

module.exports = Header;