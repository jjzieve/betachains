## Description ##
App to help you keep your favorite diabetics alive by shaming them when they eat cookies!

## Production ##
Run: 
- upstart job: "start|stop|restart betachains" -> basically just gulp + "node server.js"

## TODO/Bug & Issues Request ##
- https://workflowy.com/s/Ohf7wwIUG9

## Database (example JSON schema) ##
- URI: https://betachains.firebaseio.com/
{
  "charts" : {
    "-K2YD7dMZmePYKKwtsoQ" : {
      "owner" : "72d5a080-10c5-433b-b74b-a0f1798740fe"
    }
  },
  "data" : {
    "-K2YIwR7rgSOIzcTYf6g" : {
      "bg" : 120,
      "chart_id" : "-K2YD7dMZmePYKKwtsoQ",
      "datetime" : "2015-11-07 18:57:47"
    }
  },
  "followers" : {
    "-K2YU1HU_EgtQ5Yid9hZ" : {
      "chart_id" : "-K2YD7dMZmePYKKwtsoQ",
      "user_id" : "2aa37eed-789f-4bf5-8244-928a0779b243"
    }
  },
  "messages" : {
    "-K2YJuwepFXNw7DClGhW" : {
      "author" : "Jake Zieve",
      "chart_id" : "-K2YD7dMZmePYKKwtsoQ",
      "color" : "rgb(95, 109, 1)",
      "datetime" : "Sat Nov 07 2015 11:02:03 GMT-0800 (Pacific Standard Time)",
      "text" : "test"
    },
    "-K2YU5yUu9xlADdx-cxp" : {
      "author" : "Anti-Jake",
      "chart_id" : "-K2YD7dMZmePYKKwtsoQ",
      "color" : "rgb(3, 119, 9)",
      "datetime" : "Sat Nov 07 2015 11:46:33 GMT-0800 (Pacific Standard Time)",
      "text" : "test"
    }
  },
  "users" : {
    "2aa37eed-789f-4bf5-8244-928a0779b243" : {
      "chat_color" : "rgb(3, 119, 9)",
      "email" : "jjzieve@ucdavis.edu",
      "is_diabetic" : false,
      "name" : "Anti-Jake"
    },
    "72d5a080-10c5-433b-b74b-a0f1798740fe" : {
      "chart_id" : "-K2YD7dMZmePYKKwtsoQ",
      "chat_color" : "rgb(95, 109, 1)",
      "email" : "jjzieve@gmail.com",
      "is_diabetic" : true,
      "name" : "Jake Zieve"
    }
  }
}