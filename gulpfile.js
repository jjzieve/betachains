// References:
// https://hacks.mozilla.org/2014/08/browserify-and-gulp-with-react/
// https://blog.engineyard.com/2015/client-side-javascript-project-gulp-and-browserify
// http://fettblog.eu/gulp-browserify-multiple-bundles/
// https://www.npmjs.com/package/gulp-sourcemaps
var gulp = require('gulp');
var browserify = require('browserify');
var reactify = require('reactify');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var minify_css = require('gulp-minify-css');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var sourcemaps = require('gulp-sourcemaps');

var paths = {
  css: ['./public/src/css/*.css'],
  following_js: ['./public/src/js/following/following.jsx'],
  index_js: ['./public/src/js/index/index.jsx'],
  chart_js: ['./public/src/js/chart/chart.jsx'],
  auth_js: ['./public/src/js/auth.js'],
  react_components: ['./public/src/js/react_components/*.jsx']
};

gulp.task('css', function() {
  return gulp.src(paths.css)
    .pipe(minify_css())
    .pipe(rename(function(path){
        path.extname = '.min.css';
    }))
    .pipe(gulp.dest('./public/build/css/'));
});

// // Our JS task. It will Browserify our code and compile React JSX files.
gulp.task('following_js', function() {
  // Browserify/bundle the JS.
  return browserify(paths.following_js)
    .transform(reactify)
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(rename("bundle.min.js"))
    .pipe(gulp.dest('./public/build/js/following/'));
});

gulp.task('chart_js', function() {
  // Browserify/bundle the JS.
  return browserify(paths.chart_js)
    .transform(reactify)
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(rename("bundle.min.js"))
    .pipe(gulp.dest('./public/build/js/chart/'));
});

gulp.task('index_js', function() {
  // Browserify/bundle the JS.
  return browserify(paths.index_js)
    .transform(reactify)
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(rename("bundle.min.js"))
    .pipe(gulp.dest('./public/build/js/index/'));
});

gulp.task('auth_js', function(){
  return browserify(paths.auth_js)
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(rename("auth.min.js"))
    .pipe(gulp.dest('./public/build/js/'));
});

// Rerun tasks whenever a file changes.
gulp.task('watch', function() {
  gulp.watch(paths.css, ['css']);
  gulp.watch(paths.following_js, ['following_js']);
  gulp.watch(paths.chart_js, ['chart_js']);
  gulp.watch(paths.auth_js, ['auth_js']);
  gulp.watch(paths.auth_js, ['index_js']);
  //if any of the underlying components change, refresh the main page bundles
  gulp.watch(paths.react_components, ['following_js','chart_js','index_js']);
});

// The default task (called when we run `gulp` from cli)
// gulp.task('default', ['watch', 'css']);
gulp.task('default', ['watch', 'css', 'following_js', 'chart_js', 'auth_js','index_js']);