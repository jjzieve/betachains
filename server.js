//basic stuff
var express = require('express');
var app = require('express')();
var http = require('http').Server(app);
var path = require("path");
var body_parser = require('body-parser');
var cookie_parser = require('cookie-parser');

//3rd party stuff, fancy
// monitor auth state on server side? -> https://github.com/hokaccha/node-jwt-simple
var firebase = require('firebase');
var reference = new firebase("https://betachains.firebaseio.com");
var nodemailer = require('nodemailer');
var mailgun = require('nodemailer-mailgun-transport');

// create reusable transporter object using SMTP transport *need to create an app email probably from domain...
var auth = {
  auth: {
    api_key: 'key-7d0c646e986a6964b3c72f3966c34dc3',
    domain: 'sandboxa1dc15f347e748a88788962fd194e09d.mailgun.org'
  }
}
var transporter = nodemailer.createTransport(mailgun(auth));

app.use(express.static('public'));//serve css and frontend js from here
app.use(cookie_parser()); //use cookies on the backend
app.use(body_parser.json()); // for parsing application/json
app.use(body_parser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

//routes
app.get('/', function(request, response){
  response.sendFile(path.join(__dirname+'/views/index.html'));
});

app.get('/login', function(request, response){
  response.sendFile(path.join(__dirname+'/views/login.html'));
});

app.get('/register', function(request,response){
  response.sendFile(path.join(__dirname+'/views/register.html'));
});

app.get('/settings', function(request,response){
  response.sendFile(path.join(__dirname+'/views/settings.html'));
});

app.get('/change-password', function(request,response){
  response.sendFile(path.join(__dirname+'/views/change-password.html'));
});

app.get('/change-email', function(request,response){
  response.sendFile(path.join(__dirname+'/views/change-email.html'));
});

app.get('/chart/:id', function (request, response) {
  response.sendFile(path.join(__dirname+'/views/chart.html'));
});

app.get('/following', function(request, response){
  response.sendFile(path.join(__dirname+'/views/following.html'));
});

app.post("/invite",function(request, response){
  if(request.body.owner && request.body.email && request.body.uri){
    var uri = request.body.uri;
    var owner = request.body.owner;
    var mail_options = {
      to: request.body.email,
      from: "jjzieve@gmail.com",
      subject: "Betachains invitation!",
      html: "<h4>Hey there!</h4>"+
            "<p>"+owner.name+" has invited you to scrutinize their pancreas!<p>"+
            "<p>Go check em out, and click Follow if you want: "+
            "<a href='"+uri+"'>"+uri+"</a></p>"
    };
    transporter.sendMail(mail_options, function(error, info){
      if(error){
        response.status(500).send(error);
      }
      else{
        response.send(info);
      }
    });
  }
  else{
    response.status(500).send("We done goof'd!");
  }
});

app.post("/notify", function(request, response){
  if(request.body.owner && request.body.follower && request.body.bg && request.body.uri){
    var owner = request.body.owner;
    var follower = request.body.follower;
    var bg = parseInt(request.body.bg);
    var uri = request.body.uri;
    var mail_options = {
      to: follower.email,
      from: "no-reply@betachains.com", //betachains, digital ocean, personal email? 
      subject: "Betachains notification!",
    };
    var message = "<p>"+owner.name+" just posted a blood sugar of "+bg+".";
    if(bg < 80){
      message += " That's pretty low!</p>";
    }
    else if(bg > 120 && bg < 200){
      message += " That's getting kinda high!</p>";
    }
    else if(bg > 200 && bg < 300){
      message += " That's pretty high!</p>";
    }
    else if(bg > 300){
      message += " That's way too high!</p>";
    }
    else{
      message += "</p>";
    }
    message = "<h4>Hey "+follower.name+"!</h4>"+
              message+
              "<p>You can go yell at them, or edit your email settings if you don't want these notifications: <p>"+
              "<a href='"+uri+"'>"+uri+"</a><br>"+
              "<small>If you aren't busy :)</small>";
    mail_options.html = message;
    transporter.sendMail(mail_options, function(error, info){
        if(error){
            console.log(error);
            response.status(500).send(error);
        }
        else{
          console.log("Notification sent to: "+follower.email);
          response.send(info);
        }
    });
  }
  else{
    response.status(500).send("We done goof'd!");
  }
});

app.post("/notify_comment", function(request, response){
  if(request.body.owner && request.body.follower && request.body.comment && request.body.user && request.body.uri){
    var owner = request.body.owner;
    var follower = request.body.follower;
    var user = request.body.user;
    var comment = request.body.comment;
    var uri = request.body.uri;
    //checking equality based on emails, not ideal
    if(user.email !== follower.email){//don't send message to the person who just left the comment
      if(owner.email === follower.email){//send a seperate message if it's directed at the chart's owner
        var mail_options = {
          to: owner.email,
          from: "no-reply@betachains.com", //betachains, digital ocean, personal email? 
          subject: "Betachains notification!",
        };
        var message = "<h4>Hey "+owner.name+"!</h4>"+
                      "<p>"+user.name+" "+"left you a comment:</p>"+
                      "\""+comment+"\""+ 
                      "<p>Wanna responsd, go har: <p>"+
                      "<a href='"+uri+"'>"+uri+"</a><br>"+
                      "<small>If you aren't busy :)</small>";
      }
      else{//message sent to followers
        var mail_options = {
          to: follower.email,
          from: "no-reply@betachains.com", //betachains, digital ocean, personal email? 
          subject: "Betachains notification!",
        };
        var message = "<h4>Hey "+follower.name+"!</h4>"+
                      "<p>"+user.name+" "+"left a comment on "+owner.name+"'s beetus chart:</p>"+
                      "\""+comment+"\""+ 
                      "<p>Wanna responsd, go har: <p>"+
                      "<a href='"+uri+"'>"+uri+"</a><br>"+
                      "<small>If you aren't busy :)</small>";
      }
      mail_options.html = message;
      transporter.sendMail(mail_options, function(error, info){
          if(error){
              console.log(error);
              response.status(500).send(error);
          }
          else{
            console.log("Notification sent to: "+follower.email);
            response.send(info);
          }
      });
    }
  }
  else{
    response.status(500).send("We done goof'd!");
  }
});

http.listen(process.env.PORT || 3000, function(){
  console.log('listening on port: '+process.env.PORT || 3000);
});